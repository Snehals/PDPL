import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiceEnrollFormPage } from './service-enroll-form';

@NgModule({
  declarations: [
    ServiceEnrollFormPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiceEnrollFormPage),
  ],
})
export class ServiceEnrollFormPageModule {}
