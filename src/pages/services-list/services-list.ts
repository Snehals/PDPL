import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ServicesListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-services-list',
  templateUrl: 'services-list.html',
})
export class ServicesListPage {
  public isLayoutList: boolean = false;
  services: Array<{name:string,status:string, description:string, icon:string }>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  this.services = [];

  this.services.push({
      name: 'LOAN',
      status: 'APPROVED',
      description: 'You can avail loan facility. You have to submit basic details to start',
      icon: '../assets/imgs/test.jpeg'
      });

    this.services.push({
          name: 'FOREX',
          status: 'APPROVED',
          description: 'Excahnge your money',
          icon: '../assets/imgs/test.jpeg'
          });
    this.services.push({
        name: 'AIR TICKETS',
        status: 'APPROVED',
        description: 'Excahnge your money',
        icon: '../assets/imgs/test.jpeg'
  });
  this.services.push({
          name: 'FOREX',
          status: 'APPROVED',
          description: 'Excahnge your money',
          icon: '../assets/imgs/test.jpeg'
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicesListPage');
  }

  gridPage(){
    this.isLayoutList = !this.isLayoutList
  }

}
