import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesListPage } from '../services-list/services-list';
/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {


  services: Array<{name:string,status:string, description:string, icon:string }>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.services = [];

    this.services.push({
        name: 'LOAN',
        status: 'APPROVED',
        description: 'You can avail loan facility. You have to submit basic details to start',
        icon: '../assets/imgs/test.jpeg'
        });

      this.services.push({
            name: 'FOREX',
            status: 'APPROVED',
            description: 'Excahnge your money',
            icon: '../assets/imgs/test.jpeg'
            });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }
  itemTapped(event, item){
      this.navCtrl.push(ServicesListPage)
  }
}

/**export class service{
    private _name: string;
    private _cashbackValue: string;
    private _status: string;
    private _description: string;
    private _icon: string;

    constructor(private n: string, private s: string, private cashBack: string, private desc: string, private icon:string){
      this._name = n;
      this._cashbackValue = cashBack;
      this._status = s;
      this._description = desc;
      this._icon = icon;
    }

    get name(): string
    {
        return this._name;
    }

    set name(name: string)
    {
        this._name = name;
    }
}*/
